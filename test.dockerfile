FROM swift:5.1-bionic
ADD . /src
WORKDIR /src
RUN swift test
